package fr.tokazio.ui;

public interface TaskRunnerListener {

    void taskCompleted(Task task);

    void taskStarted(Task task);

    void taskAdded(Task task);

    void taskCancelled(Task task);

    void allCompleted();

    void taskRemoved(Task task);
}
