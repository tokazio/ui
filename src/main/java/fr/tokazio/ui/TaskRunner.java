package fr.tokazio.ui;

import java.util.concurrent.TimeUnit;

public interface TaskRunner {

    Task currentTask();

    TaskRunner add(final Task task);

    TaskRunner remove(final Task task);

    void addTaskRunnerListener(final TaskRunnerListener listener);

    boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException;
}
