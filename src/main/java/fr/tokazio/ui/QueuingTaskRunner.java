package fr.tokazio.ui;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class QueuingTaskRunner implements TaskRunner {

    private final ThreadPoolExecutor executor;
    private final List<TaskRunnerListener> listeners = new LinkedList<>();
    private Task currentTask;
    private int nbActive;
    private final Map<String, Future<?>> tasks = new HashMap<>();

    public QueuingTaskRunner(final int nbTask) {
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(nbTask);
    }

    @Override
    public Task currentTask() {
        return currentTask;
    }

    public void addTaskRunnerListener(final TaskRunnerListener listener) {
        if (listener != null) {
            this.listeners.add(listener);
        }
    }

    @Override
    public QueuingTaskRunner add(final Task task) {
        if (task != null) {
            final ListenedTask t = new ListenedTask(this, task);
            for (TaskRunnerListener l : listeners) {
                l.taskAdded(task);
            }
            tasks.put(task.getName(), executor.submit(t));
        }
        return this;
    }

    @Override
    public TaskRunner remove(final Task task) {
        if (task != null) {
            final Future<?> f = tasks.get(task.getName());
            if (f == null) {
                System.out.println("No Future for task " + task.getName());
            } else {
                f.cancel(true);
                tasks.remove(task.getName());
                for (TaskRunnerListener l : listeners) {
                    l.taskRemoved(task);
                }
            }
        }
        return this;
    }


    @Override
    public boolean awaitTermination(final long timeout, final TimeUnit unit) throws InterruptedException {
        return executor.awaitTermination(timeout, unit);
    }


    private void taskStarted(final Task task) {
        nbActive++;
        this.currentTask = task;
        for (TaskRunnerListener l : listeners) {
            l.taskStarted(task);
        }
    }

    private void taskCompleted(final Task task) {
        nbActive--;
        for (TaskRunnerListener l : listeners) {
            l.taskCompleted(task);
        }
        if (nbActive == 0) {
            for (TaskRunnerListener l : listeners) {
                l.allCompleted();
            }
            executor.shutdownNow();
        }
    }


    private void taskCancelled(final Task task) {
        nbActive--;
        for (TaskRunnerListener l : listeners) {
            l.taskCancelled(task);
        }
        if (nbActive == 0) {
            for (TaskRunnerListener l : listeners) {
                l.allCompleted();
            }
            executor.shutdown();
        }
    }


    private static class ListenedTask implements Runnable {

        private final Task task;
        private final QueuingTaskRunner runner;

        public ListenedTask(final QueuingTaskRunner runner, final Task task) {
            this.task = task;
            this.runner = runner;
        }

        @Override
        public void run() {
            runner.taskStarted(task);
            try {
                task.start();
                runner.taskCompleted(task);
            } catch (InterruptedException e) {
                runner.taskCancelled(task);
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ListenedTask that = (ListenedTask) o;
            return task.equals(that.task);
        }

        @Override
        public int hashCode() {
            return Objects.hash(task);
        }
    }


}
