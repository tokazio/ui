package fr.tokazio.ui;

public interface TaskListener {

    void statusChanged(Task task, String status);

    void minChanged(Task task, int min);

    void maxChanged(Task task, int max);

    void progressChanged(Task task, int position);

    void started(Task task);

    void completed(Task task);

    void cancelled(Task task);
}
