package fr.tokazio.ui;

import fr.tokazio.ui.swing.ProgressFrame;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class DemoProgress {

    public static void main(String[] args) throws InterruptedException {

        final TaskRunner runner = new QueuingTaskRunner(3);

        final ProgressFrame frame = new ProgressFrame(null, "Demo progress");
        frame.setPrefferedSize(new Dimension(250, 500));
        runner.addTaskRunnerListener(frame);


        frame.addTaskCancelListener(task -> {
            task.status("Annulation...");
            runner.remove(task);
        });

        for (int i = 0; i < 10; i++) {
            runner.add(a("Task #" + i));
        }

        frame.show();

        runner.awaitTermination(1, TimeUnit.MINUTES);
    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    private static Task a(final String name) {
        return new ProgressTask(name) {

            @Override
            public void execute() throws InterruptedException {
                int r = random(10000, 30000);
                System.out.println("Task " + name + " for " + r + "ms");
                status("En cours...");
                min(0);
                max(r);
                for (int i = 0; i < r; i++) {
                    Thread.sleep(1);
                    progress(i);
                }
                status("Terminée.");
            }


        }.status("En attente...");
    }
}
