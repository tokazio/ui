package fr.tokazio.ui.swing;

import javax.swing.*;
import java.awt.*;

public class ExFrame implements ComponentEx {

    private final JFrame frame = new JFrame();

    private final Component parent;

    public ExFrame(final Component parent, final String title) {
        this.parent = parent;
        frame.setTitle(title);
        frame.setDefaultCloseOperation(parent == null ? WindowConstants.EXIT_ON_CLOSE : WindowConstants.DISPOSE_ON_CLOSE);
        frame.setBackground(Colors.back);
    }

    public ExFrame setBackground(final Color color) {
        frame.setBackground(color);
        return this;
    }

    public void show() {
        frame.pack();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);
    }

    public void hide() {
        frame.setVisible(false);
        if (frame.getDefaultCloseOperation() == WindowConstants.EXIT_ON_CLOSE) {
            System.exit(0);
        }
    }

    public ExFrame add(final Component comp, final Object info) {
        frame.add(comp, info);
        return this;
    }

    public ExFrame repaint() {
        frame.repaint();
        return this;
    }

    public ExFrame revalidate() {
        frame.revalidate();
        return this;
    }


    @Override
    public java.awt.Component asComponent() {
        return frame;
    }

    public ExFrame setPrefferedSize(final Dimension dimension) {
        frame.setPreferredSize(dimension);
        return this;
    }
}
