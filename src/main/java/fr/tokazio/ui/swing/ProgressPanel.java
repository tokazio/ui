package fr.tokazio.ui.swing;

import fr.tokazio.ui.Task;
import fr.tokazio.ui.TaskListener;

import javax.swing.*;
import java.awt.*;

public class ProgressPanel extends ExPanel implements TaskListener {

    private final JPanel p = new JPanel();
    private final JLabel title = new JLabel();
    private final JLabel text = new JLabel();
    private final AnimatedProgressBar pgb = new AnimatedProgressBar();
    private final XButton cancel = new XButton();

    public ProgressPanel(final ProgressFrame parent, final Task task) {
        super();
        parent.add(comp);
        comp.setLayout(new GridLayout(3, 1));
        comp.setBackground(Colors.back);
        comp.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY));
        comp.setName(task.getName());

        comp.add(title);
        title.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        title.setForeground(Colors.disabledText);
        title.setText(task.getName());

        p.setOpaque(false);
        p.setBackground(comp.getBackground());

        p.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        comp.add(p);

        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1.0;
        c.gridy = 0;

        c.gridx = 0;
        c.weightx = 0.9;
        p.add(pgb.asComponent(), c);

        c.gridx = 1;
        c.weightx = 0.1;
        p.add(cancel.asComponent(), c);

        pgb.setBorder(BorderFactory.createEmptyBorder(4, 10, 6, 10));
        pgb.setIndeterminate(true);
        pgb.setEnabled(false);

        comp.add(text);
        text.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 0));
        text.setFont(text.getFont().deriveFont(10f));
        text.setForeground(Colors.disabledText);
        text.setText(task.getStatus());

        cancel.addActionListener((ev) -> parent.fireCancel(task));
    }

    @Override
    public void statusChanged(final Task task, final String status) {
        text.setText(status);
        text.repaint();
    }

    @Override
    public void minChanged(final Task task, final int min) {
        pgb.setMinimum(min);
        pgb.repaint();
    }

    @Override
    public void maxChanged(final Task task, final int max) {
        pgb.setMaximum(max);
        pgb.setIndeterminate(false);
        pgb.repaint();
    }

    @Override
    public void progressChanged(final Task task, final int position) {
        pgb.setValue(position);
        pgb.repaint();
    }

    @Override
    public void started(final Task task) {
        pgb.setEnabled(true);
        comp.setBackground(Colors.backSelectedBlue);
        title.setForeground(Colors.enabledText);
        text.setForeground(Colors.enabledText);
    }

    @Override
    public void completed(final Task task) {
        System.out.println("[PANEL] Task " + task.getName() + " completed");
    }

    @Override
    public void cancelled(final Task task) {
        System.out.println("[PANEL] Task " + task.getName() + " cancelled");
    }

    @Override
    protected void draw(final Graphics2D g2) {
        if (isHovered()) {
            g2.setColor(Colors.semiBrighter(comp.getBackground()));
        } else {
            g2.setColor(comp.getBackground());
        }
        g2.fillRect(0, 0, comp.getWidth(), comp.getHeight());
    }

}