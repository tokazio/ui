package fr.tokazio.ui.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

public class AnimatedProgressBar extends AbstractComponentEx<JPanel> {

    final Timer timer;
    private boolean indeterminate = false;
    private int min = 0;
    private int max = 100;
    private int value = 0;
    private final Color color = Colors.frontSelectedBlue;
    private final Color disabledColor = Colors.frontDisabled;
    private int stripesPos;

    public AnimatedProgressBar() {
        applyOn(new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {
                enhance(g);
            }
        });

        timer = new Timer(1000 / 30, (ev) -> {
            stripesPos++;
            comp.repaint();
        });
        timer.start();
    }

    @Override
    protected void draw(final Graphics2D g2) {
        final int fw = comp.getWidth();
        final int fh = comp.getHeight();
        //System.out.println("AnimatedPgb "+comp.getBounds());
        final int l = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).left : 0;
        final int t = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).top : 0;
        final int r = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).right : 0;
        final int b = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).bottom : 0;
        final Rectangle bar = new Rectangle(l, t, fw - r - l, fh - b - t);
        final int arc = (int) Math.ceil(fh / 2f);
        final int stripeWidth = fh;

        final RoundRectangle2D rect = new RoundRectangle2D.Float(bar.x, bar.y, bar.width, bar.height, arc, arc);

        if (comp.isEnabled()) {
            g2.setColor(Colors.back);
            g2.fillRoundRect(bar.x, bar.y, bar.width, bar.height, arc, arc);
        }
        g2.setColor(comp.isEnabled() ? color : disabledColor);
        float percent = value / (float) (max - min);
        int w = (int) ((fw - r - l) * percent);

        //stripes in a texture
        //TODO cache it and changes its size when component is resized
        final BufferedImage bim = new BufferedImage((int) rect.getWidth(), (int) rect.getHeight(), BufferedImage.TYPE_INT_ARGB);
        final TexturePaint tp = new TexturePaint(bim, rect.getBounds2D());
        Graphics2D bimG2 = bim.createGraphics();
        bimG2.setColor(g2.getColor().darker());
        int x = stripesPos % stripeWidth * 2 - stripeWidth * 2;
        while (x < fw) {
            for (int i = 0; i < stripeWidth; i++) {
                bimG2.drawLine(x + i + bar.height, 0, x + i, (int) rect.getHeight());
            }
            x += stripeWidth * 2;
        }
        bimG2.dispose();
        if (indeterminate || w < arc) {
            g2.fillRoundRect(bar.x, bar.y, bar.width, bar.height, arc, arc);
            g2.setPaint(tp);
            g2.fillRoundRect(bar.x, bar.y, bar.width, bar.height, arc, arc);
        } else {
            g2.fillRoundRect(bar.x, bar.y + 1, w, bar.height - 1, arc, arc);
        }
        g2.setColor(Colors.semiDarker(Color.DARK_GRAY));
        g2.drawRoundRect(bar.x, bar.y, bar.width, bar.height, arc, arc);
        //end
        g2.dispose();
    }

    public void setIndeterminate(final boolean b) {
        this.indeterminate = b;
        repaint();
    }

    public void setMinimum(int min) {
        if (min > max) {
            int old = max;
            max = min;
            min = old;
        }
        this.min = min;
        repaint();
    }


    public void setMaximum(int max) {
        if (max < min) {
            int old = min;
            min = max;
            max = old;
        }
        this.max = max;
        repaint();
    }

    public void setValue(int position) {
        if (position < min) {
            position = min;
        }
        if (position > max) {
            position = max;
        }
        this.value = position;
        repaint();
    }

}
