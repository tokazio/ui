package fr.tokazio.ui.swing;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public abstract class AbstractComponentEx<COMPONENT extends JComponent> implements ComponentEx, MouseListener, MouseMotionListener {

    protected COMPONENT comp;

    protected void applyOn(final COMPONENT comp) {
        this.comp = comp;
        comp.setOpaque(false);
        comp.setBorder(BorderFactory.createEmptyBorder());
        comp.addMouseListener(this);
        comp.addMouseMotionListener(this);
    }

    protected void enhance(final Graphics g) {
        final Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        draw(g2);
    }

    protected boolean isHovered() {
        final Point p = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(p, comp);
        return comp.contains(p.x, p.y);
    }

    protected abstract void draw(final Graphics2D g2);

    public void setPreferredSize(final Dimension dimension) {
        comp.setPreferredSize(dimension);
    }

    public void setBorder(final Border border) {
        comp.setBorder(border);
        repaint();
    }

    public void setEnabled(final boolean b) {
        comp.setEnabled(b);
        repaint();
    }

    public void repaint() {
        comp.repaint();
    }

    public void setMaximumSize(final Dimension dimension) {
        comp.setMaximumSize(dimension);
    }

    public void setMinimumSize(final Dimension dimension) {
        comp.setMinimumSize(dimension);
    }


    @Override
    public void mouseClicked(final MouseEvent e) {

    }

    @Override
    public void mousePressed(final MouseEvent e) {

    }

    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    @Override
    public void mouseEntered(final MouseEvent e) {
        comp.repaint();
    }

    @Override
    public void mouseExited(final MouseEvent e) {
        comp.repaint();
    }

    @Override
    public Component asComponent() {
        return comp;
    }

    @Override
    public void mouseDragged(final MouseEvent e) {

    }

    @Override
    public void mouseMoved(final MouseEvent e) {
        comp.repaint();
    }
}
