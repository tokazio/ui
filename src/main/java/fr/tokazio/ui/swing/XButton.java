package fr.tokazio.ui.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class XButton extends AbstractComponentEx<JButton> {

    public XButton() {
        applyOn(new JButton() {

            @Override
            protected void paintComponent(Graphics g) {
                enhance(g);
            }
        });
        comp.setRolloverEnabled(true);
    }

    @Override
    protected void draw(final Graphics2D g2) {
        final int fw = comp.getWidth();
        final int fh = comp.getHeight();
        final int l = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).left : 0;
        final int t = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).top : 0;
        final int r = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).right : 0;
        final int b = comp.getBorder() != null ? comp.getBorder().getBorderInsets(comp).bottom : 0;
        final Rectangle but = new Rectangle(l, t, fw - r - l, fh - b - t);
        Color back = Colors.buttonBack;
        Color front = comp.getParent().getBackground();
        if (comp.getModel().isArmed() || comp.getModel().isPressed()) {
            front = Colors.buttonFrontPressed;
            back = Colors.backSelectedBlue;
        } else if (comp.getModel().isRollover()) {
            front = Colors.frontSelectedBlue;
            back = Colors.buttonBackHovered;
        }
        //Filled
        g2.setColor(back);
        g2.fillOval(0, 0, fh, fh);
        //X
        g2.setColor(front);
        g2.drawLine(fh / 4 + 1, fh / 4 + 1, fh - fh / 4 - 2, fh - fh / 4 - 2);
        g2.drawLine(fh - fh / 4 - 2, fh / 4 + 1, fh / 4 + 1, fh - fh / 4 - 2);
        //end
        g2.dispose();
    }

    public void addActionListener(final ActionListener actionListener) {
        comp.addActionListener(actionListener);
    }

}
