package fr.tokazio.ui.swing;

import fr.tokazio.ui.Task;
import fr.tokazio.ui.TaskCancelListener;
import fr.tokazio.ui.TaskRunnerListener;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class ProgressFrame extends ExFrame implements TaskRunnerListener {

    private final JPanel mainPanel = new JPanel();
    protected final List<TaskCancelListener> listeners = new LinkedList<>();

    public ProgressFrame(final Component parent, final String title) {
        super(parent, title);

        mainPanel.setOpaque(true);
        mainPanel.setBorder(null);
        mainPanel.setLayout(new VerticalLayout());

        final JScrollPane jsp = new JScrollPane(mainPanel);
        jsp.setBackground(Color.DARK_GRAY);
        jsp.getViewport().getView().setBackground(Color.DARK_GRAY.darker());
        jsp.getViewport().getView().setForeground(Color.LIGHT_GRAY);
        jsp.setHorizontalScrollBarPolicy(jsp.HORIZONTAL_SCROLLBAR_NEVER);
        add(jsp, BorderLayout.CENTER);
    }

    public ProgressFrame addTaskCancelListener(final TaskCancelListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
        return this;
    }

    @Override
    public void taskCompleted(final Task task) {
        System.out.println("Task " + task.getName() + " completed");
        for (Component c : mainPanel.getComponents()) {
            if (c.getName().equals(task.getName())) {
                mainPanel.remove(c);
            }
        }
        revalidate();
        repaint();
    }

    @Override
    public void taskStarted(final Task task) {
        System.out.println("Task " + task.getName() + " started");
        //Mettre la tâche en cours en 1er
        /*
        for(Component c : mainPanel.getComponents()){
            if(c.getName().equals(task.getName())){
                mainPanel.remove(c);
                mainPanel.add(c,0);
            }
        }
        frame.revalidate();
        frame.repaint();
         */
    }

    @Override
    public void taskAdded(final Task task) {
        final ProgressPanel p = new ProgressPanel(this, task);
        p.setPreferredSize(new Dimension(-1, 55));
        System.out.println("Task " + task.getName() + " added");
        task.addListener(p);
    }

    @Override
    public void taskRemoved(final Task task) {
        for (Component c : mainPanel.getComponents()) {
            if (task.getName().equals(c.getName())) {
                mainPanel.remove(c);
            }
        }
        revalidate();
        repaint();
    }

    @Override
    public void taskCancelled(final Task task) {
        System.out.println("Task " + task.getName() + " cancelled");

    }

    @Override
    public void allCompleted() {
        System.out.println("Task runner, all tasks are completed");
        hide();
    }

    public ProgressFrame add(final Component component) {
        mainPanel.add(component);
        return this;
    }

    public void fireCancel(final Task task) {
        for (TaskCancelListener l : listeners) {
            l.with(task);
        }
    }
}
