package fr.tokazio.ui.swing;

import java.awt.*;

public final class Colors {

    public static Color backSelectedBlue = new Color(20, 66, 112);
    public static Color frontSelectedBlue = new Color(20, 66, 112).brighter().brighter();
    public static Color enabledText = Color.LIGHT_GRAY;
    public static Color disabledText = Color.GRAY;
    public static Color back = Color.DARK_GRAY;
    public static Color frontDisabled = Color.GRAY;
    public static Color buttonBack = Color.LIGHT_GRAY;
    public static Color buttonFrontPressed = Color.LIGHT_GRAY;
    public static Color buttonBackHovered = Color.WHITE;

    private Colors() {
        //hide
    }

    public static Color semiBrighter(Color color) {
        double FACTOR = 0.85f;
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();
        int alpha = color.getAlpha();

        /* From 2D group:
         * 1. black.brighter() should return grey
         * 2. applying brighter to blue will always return blue, brighter
         * 3. non pure color (non zero rgb) will eventually return white
         */
        int i = (int) (1.0 / (1.0 - FACTOR));
        if (r == 0 && g == 0 && b == 0) {
            return new Color(i, i, i, alpha);
        }
        if (r > 0 && r < i) r = i;
        if (g > 0 && g < i) g = i;
        if (b > 0 && b < i) b = i;

        return new Color(Math.min((int) (r / FACTOR), 255),
                Math.min((int) (g / FACTOR), 255),
                Math.min((int) (b / FACTOR), 255),
                alpha);

    }

    public static Color semiDarker(Color color) {
        double FACTOR = 0.85f;

        return new Color(Math.max((int) (color.getRed() * FACTOR), 0),
                Math.max((int) (color.getGreen() * FACTOR), 0),
                Math.max((int) (color.getBlue() * FACTOR), 0),
                color.getAlpha());
    }
}
