package fr.tokazio.ui.swing;

import javax.swing.*;
import java.awt.*;

public class ExPanel extends AbstractComponentEx<JPanel> {

    public ExPanel() {
        applyOn(new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {
                enhance(g);
            }
        });
    }

    @Override
    protected void draw(final Graphics2D g2) {
        g2.setColor(comp.getBackground());
        g2.fillRect(0, 0, comp.getWidth(), comp.getHeight());
    }
}
