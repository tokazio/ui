package fr.tokazio.ui;

import java.util.LinkedList;
import java.util.List;

public abstract class ProgressTask implements Task {

    private final String name;
    private final List<TaskListener> listeners = new LinkedList<>();
    private String status = "no-status";
    private boolean isStarted;

    public ProgressTask(final String name) {
        this.name = name != null ? name : "unamed";
    }

    @Override
    public void cancel() {
        if (isStarted) {
            Thread.currentThread().interrupt();
            for (TaskListener l : listeners) {
                l.cancelled(this);
            }
        }
    }

    @Override
    public void start() throws InterruptedException {
        isStarted = true;
        for (TaskListener l : listeners) {
            l.started(this);
        }
        execute();
        for (TaskListener l : listeners) {
            l.completed(this);
        }
    }

    @Override
    public ProgressTask addListener(final TaskListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public ProgressTask status(final String status) {
        this.status = status;
        for (TaskListener l : listeners) {
            l.statusChanged(this, status);
        }
        return this;
    }

    public ProgressTask min(final int min) {
        for (TaskListener l : listeners) {
            l.minChanged(this, min);
        }
        return this;
    }

    public ProgressTask max(final int max) {
        for (TaskListener l : listeners) {
            l.maxChanged(this, max);
        }
        return this;
    }

    public ProgressTask progress(final int position) {
        for (TaskListener l : listeners) {
            l.progressChanged(this, position);
        }
        return this;
    }
}
