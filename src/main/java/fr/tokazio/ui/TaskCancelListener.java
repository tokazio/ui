package fr.tokazio.ui;

public interface TaskCancelListener {

    void with(Task task);
}
