package fr.tokazio.ui;

public interface Task {

    String getName();

    String getStatus();

    Task addListener(TaskListener listener);

    void execute() throws InterruptedException;

    void start() throws InterruptedException;

    void cancel();

    Task status(String status);
}
